<?php
ini_set('display_errors',1);


?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>TemplateFO</title>
        <meta name = "viewport" content = "width = device-width, initial-scale = 1">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
        </head>
    <body class="p-3 mb-2 bg-success text-white">
       
    <div class="container">
            <div class="paragraph">
            <h3>Sjabloon functioneel ontwerp (FO)<p class="lead">Vul de onderstaande gegevens in. Klik daarna op OK om je FO te downloaden.</p></h3>
            </div>
             <hr class="my-4">
            <form class="form-vertical" role="form" action="#" method="POST" >
            <div class="form-group">
                <label for="Studentnummer"><strong>Wat is je studentnummer?</strong></label>
                <input type="text" class="form-control" id="stnr" name="stnr" aria-describedby="Studentnummer" placeholder="Je studentnummer">
                <!--<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
            </div>
            <div class="form-group">
                <label for="Opdrachtgever"><strong>Wie is je opdrachtgever?</strong></label>
                <input type="text" class="form-control" id="voorwie" name="voorwie" aria-describedby="Voor wie" placeholder="Wie is je opdrachtgever?" value="">
                <!--<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
            </div>
            <div class="form-group">
                <label for="Project"><strong>Naam van het project waarvoor je het FO gaat maken</strong></label>
                <input type="text" class="form-control" id="project" name="project"placeholder="Projectnaam">
            </div>
            <div class="form-group shadow-textarea">
                <label for="Omschrijving"><strong>Musthaves: deze eisen moeten in het eindresultaat terugkomen</strong></label>
                <textarea class="form-control z-depth-1" id="musthave" name="musthave"rows="3" placeholder=""></textarea>
            </div>
            <div class="form-group shadow-textarea">
                <label for="Omschrijving"><strong>Shouldhaves: deze eisen zijn zeer gewenst</strong></label>
                <textarea class="form-control z-depth-1" id="shouldhave" name="shouldhave" rows="3" placeholder=""></textarea>
            </div>
            <div class="form-group shadow-textarea">
                <label for="Omschrijving"><strong>Couldhaves: deze eisen mogen alleen aan bod komen wanneer er voldoende tijd voor is</strong></label>
                <textarea class="form-control z-depth-1" id="couldhave" name="couldhave" rows="3" placeholder=""></textarea>
            </div>
            <div class="form-group shadow-textarea">
                <label for="Omschrijving"><strong>Wouldhaves: deze eisen zullen nu niet aan bod komen maar kunnen in de toekomst interessant zijn (wensen)</strong></label>
                <textarea class="form-control z-depth-1" id="wouldhave" name="wouldhave" rows="3" placeholder=""></textarea>
            </div>
            <div class="form-group shadow-textarea">
                <label for="Planning"><strong>Planning. Geef aan hoeveel weken je nodig hebt en wat je per week gaat doen.</strong></label>
                <textarea class="form-control z-depth-1" id="planning" name="planning" rows="3" placeholder="Week 1: FO schrijven, Week 2:..."></textarea>
            </div>
           <button type="submit" class="btn btn-primary btn-lg btn-block" name="Go">Ok</button>
        </div>
    </div>
    </form>
        <?php
        
        if (isset($_POST['Go']) && !empty($_POST['stnr'])){
        
            
            echo $_POST['stnr'];
            /*
            echo $_POST['voorwie'];
            echo $_POST['project'];
            echo $_POST['musthave'];
            echo $_POST['shouldhave'];
            echo $_POST['couldhave'];
            echo $_POST['wouldhave'];
            echo $_POST['planning'];
            */
        
            $filename = 'tmp';
            $newfile= 'fo'.$_POST['stnr'].'.html';

            // Let's make sure the file exists and is writable first.
            if (is_writable($filename)) {

                // In our example we're opening $filename in append mode.
                // The file pointer is at the bottom of the file hence
                // that's where $somecontent will go when we fwrite() it.
                if (!$fp = fopen($filename, 'r')) {
                    echo 'Sorry 1`';

                    exit;
                }

                if (filesize($filename) > 0) {
                    $now= date("Y-m-d H:i:s");

                    $_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
                    $content = fread($fp, filesize($filename));
                    $necnt = str_replace('template',':.................',$content);
                    $necnt = str_replace('-datetime-',$now,$necnt);
                    $necnt = str_replace('stnr',$_POST['stnr'],$necnt);
                    $necnt = str_replace('opdr',$_POST['voorwie'],$necnt);
                    $necnt = str_replace('proj',$_POST['project'],$necnt);
                    $necnt = str_replace('must',$_POST['musthave'],$necnt);
                    $necnt = str_replace('shld',$_POST['shouldhave'],$necnt);
                    $necnt = str_replace('cold',$_POST['couldhave'],$necnt);
                    $necnt = str_replace('wold',$_POST['wouldhave'],$necnt);
                    $necnt = str_replace('plan',$_POST['planning'],$necnt);


                    if (!$fp2 = fopen($newfile, 'a')) {
                        echo 'Sorry 2`';
                        
                        exit;
                    }
                    
                    // Write $somecontent to our opened file.
                    if (fwrite($fp2, $necnt) === FALSE) {
                        echo 'Sorry 3`';
                        exit;
                    }

                }
                fclose($fp2);
                fclose($fp);

            } else {
                        echo 'Sorry 4`';
                
                exit;
            }
            
            
            
            echo '<a href="'.$newfile.'" target="_blank">Download '.$newfile.'</a>';
            
        } //end if isset
        $_POST = array();
        
        ?>
    </body>
</html>
